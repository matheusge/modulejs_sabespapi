; (function(window, document, $, undefined){
	'use strict';

	var app = (function(){
		var _private = {};
		var _public = {};

		//#### Métodos Públicos
		_public.ShowError = function(mensagem){
			var pagefn = doT.template(document.getElementById('thumbnail_info').text, undefined);
			$("#alerts").append(pagefn(mensagem));
		};

		return _public;
	})();

	window.modMessage = app;
})(window, document, $);