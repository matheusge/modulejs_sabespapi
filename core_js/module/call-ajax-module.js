; (function(window, document, $, undefined){
	'use strict';

	var app = (function(){

		var _private = {};
		var _public = {};


		//#### Metodos Privados
		_private.CallAjax = function(_url, _data, _type, _async, _cache, _contentType, _dataType, _success){
			
			///async default true
			var jqxhr = $.ajax({
					url: _url,
					data: _data,
					type: _type,
					async: _async,
					cache: _cache,
					contentType: _contentType,
					dataType: _dataType,
					error: function(data){
						modMessage.ShowError(data);
					},
					success: _success
					//context: document.body
			}).done(undefined);
			jqxhr.always(function(){undefined});
		};

		//#### Métodos Públicos
		_public.getJsonAsyncNoCache = function(url, data, sucess_function){
			_private.CallAjax(url, data, 'GET', true, false, 'application/json', "json", sucess_function);
		};


		return _public;
	})();

	window.modCallAjax = app;
})(window, document, $);