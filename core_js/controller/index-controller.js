; (function(window, document, $, undefined){
	'use strict';

	var app = (function(){
		var _private = {};
		var _public = {};

		//#### Métodos Privados
		_private.LoadThumbnail = function(data){
			if(data.length > 0){
				$(data).each(function(index, value){
					value.vl_p = value.data[0].value;
					value.vl = value.data[0].value.replace(" ", "").replace(",",".");
					var pagefn = doT.template(document.getElementById('thumbnail_info').text, undefined);
					$("#content").append(pagefn(value));
				})
			}
		};

		//#### Métodos Públicos
		_public.CarregaDadosMananciais = function(){
			moSabesp.ConsultaMananciais(_private.LoadThumbnail);
		};

		_public.Initialize = function(){
			_public.CarregaDadosMananciais();
		};

		return _public;
	})();

	window.appIndex = app;

	$(document).ready(function(){
		app.Initialize();
	});
})(window, document, $);