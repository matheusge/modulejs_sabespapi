; (function(window, document, $, undefined){
	'use strict';

	var app = (function(){
		var _private = {};
		var _public = {};

		//#### Métodos Públicos
		_public.ConsultaMananciais = function(success_function){
			modCallAjax.getJsonAsyncNoCache('https://sabesp-api.herokuapp.com/', {}, success_function);
		};

		return _public;
	})();

	window.moSabesp = app;

})(window, document, $);